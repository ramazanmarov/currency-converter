package networking

import android.content.Context
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException


class RetrofitClient {
    companion object {
        val baseUrl = "https://api.apilayer.com/"
        val apiKey = "AzZCZ4fCbYBxV7iNPJUKYhYdtT0aZmVR"

        var appContext: Context? = null

        val interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
            this.level = HttpLoggingInterceptor.Level.BODY
        }

        fun getHeaderInterceptor(): Interceptor {
            return Interceptor { chain ->
                val request =
                    chain.request().newBuilder()
                        .header("apikey", apiKey)
                        .build()

                val path = request.url.toString()
                val string = path.replace("%3F", "?")

                val newRequest = request.newBuilder()
                    .url(string)
                    .build()

                chain.proceed(newRequest)
            }
        }

        val client: OkHttpClient = OkHttpClient.Builder().apply {
            this.addInterceptor(interceptor)
            this.addInterceptor(getHeaderInterceptor())
        }.build()

        var gson = GsonBuilder()
            .setLenient()
            .create()

        fun getRetrofitInstance(): Retrofit {
            return Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        }
    }
}