package networking.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import networking.RetrofitClient
import networking.api.Api
import networking.models.ConvertResult

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository {
    companion object {
        private lateinit var apiService: Api

        fun convert(query: String): LiveData<ConvertResult>? {
            apiService = RetrofitClient.getRetrofitInstance().create(Api::class.java)

            val data = MutableLiveData<ConvertResult>()

            apiService.convertCurrency(query)
                .enqueue(object : Callback<ConvertResult> {
                override fun onResponse(call: Call<ConvertResult>, response: Response<ConvertResult>) {
                    if (response.isSuccessful) {
                        val responseObject = response.body()
                        Log.v("convert", "Success!")
                        data.value = responseObject
                    } else {
                        Log.v("convert", "Error!")
                        data.value = null
                    }
                }

                override fun onFailure(call: Call<ConvertResult>, t: Throwable) {
                    data.value = null
                    Log.v("convert fail", t.localizedMessage)
                }
            })
            return data
        }
    }
}