package networking.api

import networking.models.ConvertResult

import retrofit2.Call
import retrofit2.http.*

interface Api {
    // queryPath = convert?to=GBP&from=USD&amount=5
    @GET("currency_data/{query}")
    fun convertCurrency(@Path("query") query: String): Call<ConvertResult>

}