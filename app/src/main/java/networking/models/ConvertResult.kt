package networking.models

data class ConvertResult(
    var query: QueryData,
    var result: Double,
    var success: Boolean
)

data class QueryData(
    var amount: Double,
    var from: String,
    var to: String
)
