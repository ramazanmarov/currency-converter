package ui.converter

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import networking.models.ConvertResult
import networking.repository.Repository

class MainViewModel : ViewModel() {
    companion object {
        val instance = MainViewModel()
    }

    fun convert(query: String): LiveData<ConvertResult>? {
        val data = Repository.convert(query)
        Log.v("Converted result: ", data?.value.toString())
        return data
    }

}