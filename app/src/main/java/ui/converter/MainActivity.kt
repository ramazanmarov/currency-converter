package ui.converter

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.app.currencyconverterpro.R
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.bottom_sheet.*
import kotlinx.android.synthetic.main.bottom_sheet.view.*
import networking.models.Valute

class MainActivity: AppCompatActivity(), ItemAdapter.ItemClickListener {
    private lateinit var bottomSheetDialog: BottomSheetDialog
    private lateinit var itemAdapter: ItemAdapter
    val viewModel = MainViewModel.instance

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupView()
        setupBottomSheet()
    }

    fun setupBottomSheet(){
        val dialogView = layoutInflater.inflate(R.layout.bottom_sheet,null)
        bottomSheetDialog = BottomSheetDialog(this,R.style.BottomSheetDialogTheme)
        bottomSheetDialog.setContentView(dialogView)
        dialogView.rv_item.layoutManager = LinearLayoutManager(this)
        itemAdapter = ItemAdapter(this)
        dialogView.rv_item.adapter = itemAdapter
        dialogView.tv_cancel.setOnClickListener{
            bottomSheetDialog.dismiss()
        }
    }

    fun setupView(){
        btn_convert.setOnClickListener {
            if (et_input_sum.text.isNotEmpty()){
                val sum = et_input_sum.text.toString().toDouble()
                val fromCurs = btn_convert_from.text
                val toCurs = btn_convert_to.text

                convert("convert?to=$toCurs&from=$fromCurs&amount=$sum")
            }else{
                Toast.makeText(this, "Введите сумму", Toast.LENGTH_SHORT).show()
            }
        }

        btn_convert_from.setOnClickListener { showBottomSheet(true) }
        btn_convert_to.setOnClickListener { showBottomSheet(false) }
        iv_switch.setOnClickListener {
            val fromVal = btn_convert_from.text
            btn_convert_from.text = btn_convert_to.text
            btn_convert_to.text = fromVal
            tv_result.text = "0"
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun showBottomSheet(isActionFrom: Boolean){
        itemAdapter.isActionFromValute = isActionFrom
        itemAdapter.notifyDataSetChanged()
        bottomSheetDialog.show()
    }

    fun convert(query: String){
        dialog.isVisible = true
        viewModel.convert(query)?.observe(this) {
            dialog.isVisible = false
            if (it != null){
                tv_result.text = it.result.toString()
            }
        }
    }

    override fun onClick(item: Valute, isFrom: Boolean) {
        if (isFrom)
            btn_convert_from.text = item.codeName
        else
            btn_convert_to.text = item.codeName

        bottomSheetDialog.dismiss()
        tv_result.text = "0"
    }
}