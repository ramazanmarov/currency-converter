package ui.converter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.app.currencyconverterpro.R
import com.android.app.currencyconverterpro.databinding.NameValuteItemBinding
import networking.models.Valute


class ItemAdapter(private val clickListener: ItemClickListener): RecyclerView.Adapter<ItemAdapter.ItemHolder>() {
    private var valuteList: MutableList<Valute> = mutableListOf(
        Valute("Доллар", "USD"),
        Valute("Сом", "KGS"),
        Valute("Рубль", "RUB"),
        Valute("Евро", "EUR"),
    )
    private var selectedItemIndex: Int = 0
    var isActionFromValute = true


    override fun getItemCount(): Int {
        return valuteList.size
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.bind(valuteList[position], position, selectedItemIndex, isActionFromValute)
    }

    class ItemHolder(item: View): RecyclerView.ViewHolder(item){
        val binding = NameValuteItemBinding.bind(item)
        lateinit var item: Valute
        var index: Int = 0
        var isFromValute: Boolean = true

        fun bind(valute: Valute, position: Int, selPosition: Int, isFrom: Boolean) = with(binding){
            tvValuteName.text = valute.name
            tvValuteCode.text = valute.codeName
            item = valute
            index = position
            isFromValute = isFrom
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_valute_name,parent,false)
        return ItemHolder(view).apply { itemView.setOnClickListener{
            clickListener.onClick(item, isFromValute)
        } }
    }

//    fun setSelected(index: Int){
//        selectedItemIndex = index
//        notifyDataSetChanged()
//    }

    interface ItemClickListener{
        fun onClick(item: Valute, isFrom: Boolean)
    }
}