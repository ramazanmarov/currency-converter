package ui.splash

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import android.content.Intent
import android.os.Handler
import android.view.Window
import com.android.app.currencyconverterpro.R
import ui.converter.MainActivity


class SplashActivity : AppCompatActivity() {
    private val SPLASH_TIME_OUT: Long = 3000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        window.requestFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            val mainActivity = Intent(this@SplashActivity, MainActivity::class.java)
            startActivity(mainActivity)
            finishAffinity()
        }, SPLASH_TIME_OUT)
    }
}